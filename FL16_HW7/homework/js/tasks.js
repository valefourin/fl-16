const evenRemainder = 2, digitsRemainIndex = -4, digitNum = 10;

// 1
function getMaxEvenElement(nums) {
    let arr = nums
        .map(el => Number.parseInt(el))
        .filter(el => el % evenRemainder === 0);
    return Math.max(...arr);
}


// 2
let a = 3, b = 5;
[a, b] = [b, a];


// 3
function getValue(val) {
    return val ?? '-';
}


// 4
function getObjFromArray(arr) {
    let obj = {}
    arr.forEach(el => {
        obj[el[0]] = el[1]
    });
    return obj;
}


// 5
function addUniqueId(obj) {
    let copy = {
        id: Symbol()
    }
    return Object.assign(copy, obj)
}


// 6
function getRegroupedObject(obj) {
    const {
        name: firstName,
        details: { university, age, id}
    } = obj;
    const user = {age, firstName, id};
    return { university, user}
}


// 7
function getArrayWithUniqueElements(arr) {
    return Array.from(new Set(arr));
}


// 8 
function hideNumber(number) {
    return number.slice(digitsRemainIndex).padStart(digitNum, '*');
}


// 9
function add(a, b, ...args) {
    if (a === undefined) {
        throw new Error('a and b are required')
    } else if (b === undefined){
        throw new Error('b is required')
    } else {
        return Array(a,b,...args).reduce((res, el) => res + el)
    }
}


// 10 
function* generateIterableSequence(arr) {
    for (let el of arr) {
        yield el;
    }
}
