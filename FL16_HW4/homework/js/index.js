'use strict';

const argsNum = 2;

class Pizza { 
    constructor(size, type) {
        if (arguments.length !== argsNum) {
            throw new PizzaException('Required two arguments, given: ' + arguments.length)
        } else if (!Pizza.allowedSizes.map( el => el['name']).includes(size.name)) {
            throw new PizzaException(`${size.name} is invalid size!`);
        } else if (!Pizza.allowedTypes.map( el => el['name']).includes(type.name)) {
            throw new PizzaException(`${type.name} is invalid type!`);
        } else {
            this.size = size;
            this.type = type;
            this.extra_ingredients = [];
        }
    }

    addExtraIngredient(ingredient) {
        if (arguments.length !== 1) {
            throw new PizzaException('Must receive only one argument!');
        } else if (!Pizza.allowedExtraIngredients.map( el => el['name']).includes(ingredient.name)) {
            throw new PizzaException(`Ingredient ${ingredient.name} does not exist!`);
        } else if (this.extra_ingredients.map( el => el['name']).includes(ingredient.name)) {
            throw new PizzaException(`Ingredient ${ingredient.name} was already added!`);
        } else {
            this.extra_ingredients.push(ingredient);
        }
    }

    removeExtraIngredient(ingredient) {
        if (arguments.length !== 1) {
            throw new PizzaException('Must receive only one argument!');
        } else if (!Pizza.allowedExtraIngredients.map( el => el['name']).includes(ingredient.name)) {
            throw new PizzaException(`Ingredient ${ingredient.name} does not exist!`);
        } else if (!this.extra_ingredients.map( el => el['name']).includes(ingredient.name)) {
            throw new PizzaException(`Ingredient ${ingredient.name} was not added!`);
        } else {
            let index = this.extra_ingredients.findIndex(i => i.name === ingredient);
            this.extra_ingredients.splice(index, 1);
        }
    } 

    getSize() {
        return this.size;
    }

    getPrice() {
        let extraIngredientsPrice = 0;
        this.extra_ingredients.forEach( el => { 
            extraIngredientsPrice += el.price 
            })
        let price = this.size.price + this.type.price + extraIngredientsPrice;
        return price;
    }

    getExtraIngredients() {
        return this.extra_ingredients.map(el => el.name);
    }

    getPizzaInfo() {
        let info = 
        'size: ' + this.size.name + 
        '\ntype: ' + this.type.name + 
        '\nextra ingredients: ' + this.getExtraIngredients().join(', ') + 
        '\nprice: ' + this.getPrice() + ' UAH.';
        return info;
    }

}

//  Sizes, types and extra ingredients 
 Pizza.SIZE_S = {name: 'SMALL', price: 50}
 Pizza.SIZE_M = {name: 'MEDIUM', price: 75}
 Pizza.SIZE_L = {name: 'LARGE', price: 100}
 Pizza.TYPE_VEGGIE = {name: 'VEGGIE', price: 50}
 Pizza.TYPE_MARGHERITA = {name: 'MARGHERITA', price: 60}
 Pizza.TYPE_PEPPERONI = {name: 'PEPPERONI', price: 70}
 Pizza.EXTRA_TOMATOES = {name: 'TOMATOES', price: 7}
 Pizza.EXTRA_CHEESE = {name: 'CHEESE', price: 5}
 Pizza.EXTRA_MEAT = {name: 'MEAT', price: 9}

 /* Allowed properties */
 Pizza.allowedSizes = [
    Pizza.SIZE_S, 
    Pizza.SIZE_M, 
    Pizza.SIZE_L 
]
 Pizza.allowedTypes = [
    Pizza.TYPE_VEGGIE,
    Pizza.TYPE_MARGHERITA,
    Pizza.TYPE_PEPPERONI
]
 Pizza.allowedExtraIngredients = [
    Pizza.EXTRA_TOMATOES,
    Pizza.EXTRA_CHEESE,
    Pizza.EXTRA_MEAT
]


 function PizzaException(errorMsg) { 
    this.log = errorMsg;
  }

