// Your code goes here

// calendar
window.onload = function () {
    let form = document.getElementById('form'),
    personName = document.getElementById('name'),
    time = document.getElementById('time'),
    place = document.getElementById('place'),
    submitBtn = document.getElementById('submit');

    let eventName = prompt('Input event name: ', '');
    document.getElementById('eventName').textContent = eventName;
    form.style.visibility = 'visible';

    let validateForm = function() {
        let inputs = document.getElementsByTagName('input');
        let validated = true;
        let datePattern = /^(2[0-3]|[01]?[0-9]):([0-5]?[0-9])$/;
        for (let input of inputs) {
            if (input.value === '') {
                validated = false;
                alert(`Input all data in input ${input.id}!`);
            }
        }
        if (datePattern.test(time.value) === false) {
            validated = false;
            alert('Incorrect time! Enter time in format hh:mm');
        }
        if (validated === true) {
            let msg = `${personName.value} has a ${eventName} at ${time.value} ${place.value}`;
            console.log(msg);
            alert(msg);
        }
    }

    submitBtn.addEventListener('click', validateForm, false);
   

    // converter
    let convertBtn = document.getElementById('convertBtn');
    let amountEuro = 0, amountDollar = 0;
    let amountEuroToHrn = 0, amountDollarToHrn = 0;
    let euroExchangeRate = 33.56, dollarExchangeRate = 27.46;
    let fixedPoint = 2;

    let convert = function() {
        do {
            amountEuro = +prompt('Input amount of euro: ', 0);
            if (Number.isInteger(amountEuro) === false) {
                alert('Input integers!');
            } else {
                if (amountEuro < 0) {
                    alert('The amount should be positive number!')
                }
            }
        } while (Number.isInteger(amountEuro) === false);

        do {
            amountDollar = +prompt('Input amount of dollar: ', 0);
            if (Number.isInteger(amountDollar) === false) {
                alert('Input integers!');
            } else {
                if (amountDollar < 0) {
                    alert('The amount should be positive number!')
                }
            }
        } while (Number.isInteger(amountDollar) === false);

        amountEuroToHrn = amountEuro * euroExchangeRate;
        amountDollarToHrn = amountDollar * dollarExchangeRate;

        alert(`${amountEuro} euros are equal ${amountEuroToHrn.toFixed(fixedPoint)} hrns, 
${amountDollar} dollars are equal ${amountDollarToHrn.toFixed(fixedPoint)} hrns.`);
    }

    convertBtn.addEventListener('click', convert, false);
}