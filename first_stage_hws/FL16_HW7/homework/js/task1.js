// Your code goes here

let moneyAmount, yearNumber, percentage, totalProfit = 0, currentProfit = 0, totalAmount;
const thousand = 1000, hundred = 100, two = 2;


do {
    moneyAmount = +prompt('Input amount of money:', 0);
    if (Number.isInteger(moneyAmount) === true) {
        if (moneyAmount < thousand) {
            alert("Initial amount can't be less than 1000!")
        } else {
            continue;
        }
    } else {
        alert('Invalid input data!');
    }
} while (Number.isInteger(moneyAmount) === false);

do {
    yearNumber = +prompt('Input number of years:', 0);
    if (Number.isInteger(yearNumber) === true) {
        if (yearNumber < 1) {
            alert("Number of years can't be less than 1!")
        } else {
            continue;
        }
    } else {
        alert('Invalid input data!');
    }
} while (Number.isInteger(yearNumber) === false) 

do {
    percentage = +prompt('Input percentage of a year:', 0);
    if (Number.isInteger(percentage) === true) {
        if (percentage > hundred && percentage < 0) {
            alert("Percentage can't be bigger than 100 or smaller than 0!")
        } else {
            continue;
        }
    } else {
        alert('Invalid input data!');
    }
} while (Number.isInteger(percentage) === false) 


for (let i = 0; i < yearNumber; i++) {
    totalProfit = currentProfit + (moneyAmount + totalProfit) * (percentage / hundred);
    totalAmount = moneyAmount + totalProfit;
    currentProfit = totalProfit;
}

alert(`\nTotal profit: ${totalProfit.toFixed(two)}\nTotal amount: ${totalAmount.toFixed(two)}`);