let userConfirmation = confirm('Do you want to play a game?'); 
let winPocket, userGuess, maxPocketNumber = 4; 
let maxPrize = 100, userPrize = 0;
let currentGame = 1;
const nine = 9, four = 4, three = 3, two = 2;

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; 
  }

if (userConfirmation) {
    while (userConfirmation) {
        maxPocketNumber += four;
        maxPrize *= two;
        let currentPrize = maxPrize;
        winPocket = getRandomInt(0, maxPocketNumber + 1);
        
        let userContinueConfirmation;
        for (let i = 1; i < three + 1; i++) {
            
            if (i === two || i === three) {
                currentPrize /= two;
            }

            userGuess = +prompt(`
                Choose a roulette pocket number from 0 to ${maxPocketNumber} ${winPocket}\n
                Attempts left: ${three + 1 - i}\n
                Total prize: ${userPrize} $\n
                Possible prize on current attempt: ${currentPrize} $`);

            if (userGuess === winPocket) {
                i = three;
                continue;
            }
        }

        if (userGuess === winPocket) {
            userPrize = userPrize + currentPrize;
            userContinueConfirmation = confirm(
                `Congratulation, you won! Your prize is: ${userPrize} $. Do you want to continue?`);
            if (userContinueConfirmation) {
                currentGame++;
                continue;
            } else {
                alert(`Exit with total prize ${userPrize}$`);
                break;
            }
        }
            userContinueConfirmation = confirm('You did not become a billionaire, but you can try again.');
            if (userContinueConfirmation) {
                currentGame++;
                continue;
            } else {
                alert('Exit.');
                break;
            }
    }
} else {
    alert('You lost your chance to become a billionaire!');
}