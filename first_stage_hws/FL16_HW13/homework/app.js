const appRoot = document.getElementById('app-root');
const negativeSortValue = -1;

let header = document.createElement('header'), 
        h1 = document.createElement('h1'),
        form = document.createElement('form'),
        parRadio = document.createElement('p'),
        labelTypeOfSearch = document.createElement('label'),
        radioRegion = document.createElement('input'),
        labelByRegion = document.createElement('label'),
        radioLang = document.createElement('input'),
        labelByLang = document.createElement('label'),

        parSelect = document.createElement('p'),
        labelSearchQuery = document.createElement('label'),
        selectRegion = document.createElement('select'),
        selectLang = document.createElement('select'),
        defaultOption = document.createElement('option');

let table = document.createElement('table'),
    thead = document.createElement('thead'),
    tbody = document.createElement('tbody');

let tableTitles = ['Country Name', 'Capital', 'World Region', 'Languages', 'Area', 'Flag'];

let countryNameTitle, areaTitle;

let regionsList = externalService.getRegionsList(),
    languagesList = externalService.getLanguagesList(),
    countriesList;

let query;
let noQueryChosenMsg = document.createElement('label');

function renderHeader() {

    h1.innerText = 'Countries Search';
    labelTypeOfSearch.innerText = 'Please choose type of search:';
    labelByRegion.innerText = 'By Region';
    labelByLang.innerText = 'By Language';
    defaultOption.innerText = 'Select value';
    
    radioRegion.type = 'radio';
    radioLang.type = 'radio';
    radioRegion.id = 'region';
    radioLang.id = 'lang';
    radioRegion.name = 'countrySearch';
    radioLang.name = 'countrySearch';
    labelByRegion.for = 'region';
    labelByLang.for = 'lang';
    header.className = 'header';

    selectRegion.setAttribute('disabled',''); 
    selectLang.className = 'hidden';
    defaultOption.setAttribute('value','select');
    defaultOption.setAttribute('disabled', '');
    defaultOption.setAttribute('selected','');
    labelSearchQuery.innerText = 'Please choose search query:';

    parRadio.appendChild(labelTypeOfSearch);
    parRadio.appendChild(radioRegion);
    parRadio.appendChild(labelByRegion);
    parRadio.appendChild(radioLang);
    parRadio.appendChild(labelByLang);
    form.appendChild(parRadio);

    selectRegion.appendChild(defaultOption);
    
    renderList(regionsList, selectRegion);
    renderList(languagesList, selectLang);

    parSelect.appendChild(labelSearchQuery);
    parSelect.appendChild(selectRegion);
    parSelect.appendChild(selectLang);

    form.appendChild(parSelect);
    header.appendChild(h1);
    header.appendChild(form);
    appRoot.appendChild(header);

}

function renderList(list, container) {
    list.forEach(el => {
        let option = document.createElement('option');
        option.innerText = el;
        option.value = el;
        container.appendChild(option)
    });
}

function changeOptionList() {
    query = undefined;
    table.classList.add('hidden');
    noQueryChosenMsg.classList.remove('hidden');

    if (selectRegion.disabled) {
        selectRegion.removeAttribute('disabled');
    }

    if (radioRegion.checked === true) {
        selectRegion.appendChild(defaultOption);
        selectLang.className = 'hidden';
        selectRegion.classList.remove('hidden');
    } else if (radioLang.checked === true) {
        selectLang.appendChild(defaultOption);
        selectRegion.className = 'hidden';
        selectLang.classList.remove('hidden');
    }
}

function getSelectedSearchQuery(e) {
    query = e.target.value;
    defineCountriesList(query);
    renderTableContent();
}

function sortStringsInAscend(arr) {
    function compareCountryNames(a, b) {
        if (a.name < b.name) {
            return negativeSortValue;
        }
        if (a.name > b.name) {
            return 1;
        }
        return 0;
    }
    return arr.sort(compareCountryNames);
}

function sortStringsInDescend(arr) {
    function compareCountryNames(a, b) {
        if (a.name > b.name) {
            return negativeSortValue;
        }
        if (a.name < b.name) {
            return 1;
        }
        return 0;
    }
    return arr.sort(compareCountryNames);
}

function sortAreasInAscend(arr) {
    return arr.sort((a,b) => a.area-b.area);

}

function sortAreasInDescend(arr) {
    return arr.sort((a,b) => b.area-a.area);
}

function renderTableTitles() {
    let tr = document.createElement('tr');
    for (let el of tableTitles) {
        let th = document.createElement('th');
        th.innerText = el;

        if (el === 'Country Name') {
            th.classList.add('sortInAscend');
            countryNameTitle = th;
        }
        if (el === 'Area') {
            th.classList.add('sortNotApplied');
            areaTitle = th;
        }

        tr.appendChild(th);
    }
    thead.appendChild(tr);
}

function renderTable() {
    noQueryChosenMsg.innerText = 'No items, please choose search query';
    noQueryChosenMsg.classList.add('hidden');
    noQueryChosenMsg.classList.add('noQueryChosenMsg');
    table.classList.add('hidden');
    table.classList.add('table');
    table.appendChild(thead);
    table.appendChild(tbody);
    appRoot.appendChild(table);
    appRoot.appendChild(noQueryChosenMsg);
    renderTableTitles();
}

function defineCountriesList(param) {
    if (radioRegion.checked === true) {
        countriesList = sortStringsInAscend(externalService.getCountryListByRegion(param));
    } else if (radioLang.checked === true) {
        countriesList = sortStringsInAscend(externalService.getCountryListByLanguage(param));
    }
}

function countriesSortHandler() {
    if (countryNameTitle.classList.contains('sortInAscend')) {
        countryNameTitle.className = 'sortInDescend';
        areaTitle.className = 'sortNotApplied';
        countriesList = sortStringsInDescend(countriesList);
    } else if (countryNameTitle.classList.contains('sortInDescend')) {
        countryNameTitle.className = 'sortInAscend';
        areaTitle.className = 'sortNotApplied';
        countriesList = sortStringsInAscend(countriesList);
    } else {
        countryNameTitle.className = 'sortInAscend';
        areaTitle.className = 'sortNotApplied';
        countriesList = sortStringsInAscend(countriesList);
    }
    renderTableContent();
}

function areasSortHandler() {
    if (areaTitle.classList.contains('sortInAscend')) {
        areaTitle.className = 'sortInDescend';
        countryNameTitle.className = 'sortNotApplied';
        countriesList = sortAreasInDescend(countriesList);
    } else if (areaTitle.classList.contains('sortInDescend')) {
        areaTitle.className = 'sortInAscend';
        countryNameTitle.className = 'sortNotApplied';
        countriesList = sortAreasInAscend(countriesList);
    } else {
        areaTitle.className = 'sortInAscend';
        countryNameTitle.className = 'sortNotApplied';
        countriesList = sortStringsInAscend(countriesList);
    }
    renderTableContent();
}

function renderTableContent() {
    if (tbody.innerHTML !== '') {
        tbody.innerHTML = '';
    }

    noQueryChosenMsg.classList.add('hidden');
    table.classList.remove('hidden');

    for (let el of countriesList) {
        let tr = document.createElement('tr');
        let tdCountry = document.createElement('td');
        let tdCapital = document.createElement('td');
        let tdRegion = document.createElement('td');
        let tdLang = document.createElement('td');
        let tdArea = document.createElement('td');
        let tdFlag = document.createElement('td');
        let tdFlagImg = document.createElement('img');

        tdCountry.innerText = el.name;
        tdCapital.innerText = el.capital;
        tdRegion.innerText = el.region;
        tdLang.innerText = Object.values(el.languages);
        tdArea.innerText = el.area;
        tdFlag.appendChild(tdFlagImg);
        tdFlagImg.setAttribute('src', el.flagURL);

        tr.appendChild(tdCountry);
        tr.appendChild(tdCapital);
        tr.appendChild(tdRegion);
        tr.appendChild(tdLang);
        tr.appendChild(tdArea);
        tr.appendChild(tdFlag);

        tbody.appendChild(tr);
    }
}

renderHeader();
renderTable();

parRadio.addEventListener('change', changeOptionList, false);
selectRegion.addEventListener('change', getSelectedSearchQuery,false); 
selectLang.addEventListener('change', getSelectedSearchQuery,false); 
countryNameTitle.addEventListener('click', countriesSortHandler, false);
areaTitle.addEventListener('click', areasSortHandler, false);