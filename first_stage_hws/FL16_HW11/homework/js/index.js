function visitLink(path) {
	let pathValue = Number(localStorage[path]);

	if (localStorage.getItem(path) === null) {
		pathValue = 0;
	}

	localStorage.setItem(path.toString(), localStorage[path.toString()] = pathValue + 1);
}

function viewResults() {
	let storageData = [];

	for (let i = 0; i < localStorage.length; i++) {
		storageData[i] = localStorage.key(i);
	}

	let content = document.getElementById('content');
	let result = document.createElement('ul');

	for (let j = 0; j < storageData.length; j++) {
		result.innerHTML += 
		`<li>You visited ${storageData[j]} ${localStorage.getItem(localStorage.key(j))} time(s)</li>`;
	}

	content.appendChild(result);
	localStorage.clear();
}
 