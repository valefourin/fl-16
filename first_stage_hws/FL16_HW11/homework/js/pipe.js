function isFunction(functionToCheck) {
	return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}

const pipe = (value, ...funcs) => {
	let functionsArray = funcs;
	let currentResult = value;
	try {
		for (let i in functionsArray) {
			if ({}.hasOwnProperty.call(functionsArray, i)) {
				if (isFunction(functionsArray[i]) === true) {
					currentResult = functionsArray[i](currentResult);
				} else {
					throw `Provided argument ${functionsArray[i]} at position ${i} is not a function!`;
				}
			}
		}
	} catch (e) {
		return e;
	}	

	return currentResult;
};

const replaceUnderscoreWithSpace = (value) => value.replace(/_/g, ' ');
const capitalize = (value) =>
	value
		.split(' ')
		.map((val) => val.charAt(0).toUpperCase() + val.slice(1))
		.join(' ');
const appendGreeting = (value) => `Hello, ${value}!`;

