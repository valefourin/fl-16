const daysInAYear = 365, hoursInADay = 24, minutesInAnHour = 60, milisecondsInASecond = 1000;
const oneDay = hoursInADay * minutesInAnHour * minutesInAnHour * milisecondsInASecond;
const oneYear = oneDay * daysInAYear;
const nonExistingValue = -1

function getAge(date) {
    let dateStart = Date.parse(date);
    let dateNow = Date.now();
    let years = Math.round(Math.abs(dateNow - dateStart) / oneYear);
    return years;
}

function getWeekDay(date) {
    let weekDayString = date.toLocaleDateString('en-ZA', {weekday: 'long'});
    return weekDayString;
}

function getAmountDaysToNewYear() {
    let todayDate = new Date();
    let NewYear = new Date(+todayDate.getFullYear() + 1, 1, 1);
    let days = Math.round(Math.abs(NewYear - todayDate) / oneDay);
    return days;
}

function getProgrammersDay(year) {
    const progDayNumberInYear = 255;
    const progDayInMilisec = progDayNumberInYear * 
    hoursInADay * minutesInAnHour * minutesInAnHour * milisecondsInASecond;
    let currentYear = new Date(year,0);
    let progDay = new Date(currentYear.getTime() + progDayInMilisec);
    let resultString = progDay.toLocaleDateString('en-ZA', {day: '2-digit', month: 'short', year: 'numeric'}) 
    + ' (' + getWeekDay(progDay) + ')';
    return resultString;
}

function howFarIs(specifiedWeekday) {
    let weekdays = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
    let today = new Date().getDay() - 1;
    let specifiedWeekdayIndex = nonExistingValue;

    for (let i in weekdays) {
        if ({}.hasOwnProperty.call(weekdays, i)) {
            if (specifiedWeekday.toLocaleLowerCase() === weekdays[i].toLocaleLowerCase()) {
                specifiedWeekdayIndex = Number(i);
            } 
        }
    }

    if (specifiedWeekdayIndex === nonExistingValue) {
        console.error('Inputed wrong value!');
        return 'Inputed wrong value!';
    } else if (specifiedWeekdayIndex === today) {
        return `Hey, today is ${ weekdays[specifiedWeekdayIndex] } =)`
    } else {
        let specifiedNextWeekdayIndex = specifiedWeekdayIndex;
        if (specifiedWeekdayIndex < today) {
            specifiedNextWeekdayIndex = specifiedWeekdayIndex + weekdays.length;
        }
        let daysLeft = specifiedNextWeekdayIndex - today;
        return `It's ${ daysLeft } day(s) left till ${ weekdays[specifiedWeekdayIndex] }`;
    }
}

function isValidIdentifier(str) {
    let validIdPattern = /^[a-zA-Z][a-zA-Z0-9_$]*$/;
    let result = validIdPattern.test(str);
    return result;
}

function capitalize(str) {
    let firstLetterPattern = /(^\w{1})|(\s+\w{1})/g;
    return str.replace(firstLetterPattern, letter => letter.toUpperCase());
}

function isValidAudioFile(fileName) {
    let validFileNamePattern = /^[a-zA-Z]+\.((mp3)|(flac)|(alac)|(aac))$/g;
    return validFileNamePattern.test(fileName)
}

function getHexadecimalColors(str) {
    let hexColorPattern = /^((#[0-9a-f]{6})*|#([0-9a-f]{3})*|(\s+#[0-9a-f]{6})*|(\s+#[0-9a-f]{3})*)+$/gi;
    let colors = str.match(hexColorPattern);

    if (colors === null) {
        colors = 'One or all of the hex colors are invalid!';
    } else {
        colors = colors[0].split(' ');
    }

    return colors;
}

function isValidPassword(str) {
    let validPassPattern = /^(?=.*[\d])(?=.*[a-z])(?=.*[A-Z])[a-zA-Z0-9]{8,}$/;
    return validPassPattern.test(str);
}

function addThousandsSeparators(str) {
    let numberPattern = /^[\d]+/, thirdNumberPositionPattern = /\B(?=(\d{3})+(?!\d))/g;
    let resultStr;

    if (typeof str === 'string') {
        resultStr = str.match(numberPattern)[0];
        if (resultStr === null) {
            return 'Error! Cannot get number from the string!';
        }
    } else if (typeof str === 'number') {
        resultStr = str.toString();
    }
    
    return resultStr.replace(thirdNumberPositionPattern, ',');
}


function getAllUrlsFromText(str) {
    let urlPattern = /(https:\/\/)[a-zA-Z0-9]+\.[a-zA-Z0-9]+/g;
    return str.match(urlPattern);
}
