/* START TASK 1: Your code goes here */
const notificationTimeout = 3000, mousePositionDeviation = 20;
let cells = document.getElementsByTagName('td');
let specialCell = document.getElementById('specialCell');
let tableRows = document.getElementsByTagName('tr');

function specialCellEvent() {
    for (let cell of cells) {
        cell.className = '';
        cell.classList.add('yellow');
    }
}

function clickCellHandler() {
    if (this.classList.contains('green')) {
        this.classList.remove('green');
        this.classList.add('yellow');
    } else if (this.classList.contains('yellow')) {
        this.classList.remove('yellow');
        this.classList.add('green');
    } else if (this.classList.contains('green') === false) {
        this.classList.remove('green');
        this.classList.add('yellow');
    }
}

function clickFirstColumnCellHandler(i) {   
    for (let j = 0; j < tableRows[i].children.length; j++) {
        if (tableRows[i].children[j].classList.contains('yellow')) {
            console.log();
        } else {
            tableRows[i].children[j].className = '';
            tableRows[i].children[j].classList.add('blue');
        }
    }
}

specialCell.addEventListener('click', specialCellEvent, false);

for (let cell of cells) {
    cell.addEventListener('click', clickCellHandler, false);
}

for (let i = 0; i < tableRows.length; i++) {
        tableRows[i].children[0].removeEventListener('click', clickCellHandler)
        tableRows[i].children[0].addEventListener('click', function() { 
            clickFirstColumnCellHandler(i) 
        }, false);
}

/* END TASK 1 */

/* START TASK 2: Your code goes here */

let field = document.getElementById('field'),
    btn = document.getElementById('btn'),
    notification = document.getElementById('notification');

let numberPattern = /\+(380)[0-9]{9}/;

function validateInput() {
    if (field.validity.valid) {
        notification.classList.add('hidden');
        btn.removeAttribute('disabled');
    } else {
        notification.innerText = 'Type number does not follow format +380*********';
        notification.classList.remove('hidden');
        notification.classList.add('notification-invalid');
        btn.setAttribute('disabled', '');
    }
}

function sendData() {
    notification.innerText = 'Data was successfully sent';
    notification.classList.remove('hidden');
    notification.classList.remove('notification-invalid');
    notification.classList.add('notification-valid');
}

field.addEventListener('change', validateInput, false);
btn.addEventListener('click', sendData, false);

/* END TASK 2 */

/* START TASK 3: Your code goes here */

let court = document.getElementById('court'),
    ball = document.getElementById('ball'),
    aTeamScoringZone = document.getElementById('aTeamScoringZone'),
    bTeamScoringZone = document.getElementById('bTeamScoringZone'),
    aTeamScore = document.getElementById('aTeamScore'),
    bTeamScore = document.getElementById('bTeamScore'),
    scoreNotification = document.getElementById('scoreNotification');
let teamA = 'a', teamB = 'b';

function restoreBallPosition() {

    ball.classList.add('position-before-first-click');
    ball.style.left = '280px';
    ball.style.top = '110px';
}

function moveBall(e) {
    if (ball.classList.contains('position-before-first-click')) {
        ball.classList.remove('position-before-first-click');
        ball.classList.add('position-after-first-click');
    }
    ball.style.left = e.clientX - mousePositionDeviation + 'px';
    ball.style.top = e.clientY - mousePositionDeviation + 'px';
}

function teamScored(team) {
    if (team === 'a') {
        aTeamScore.innerText = Number(aTeamScore.innerText) + 1;
    } else if (team === 'b'){
        bTeamScore.innerText = Number(bTeamScore.innerText) + 1;
    }
    let scoredEvent = new CustomEvent('scored', {
        bubbles: true,
        detail: {
            teamWinner: team
        }
    });
    scoreNotification.addEventListener('scored', (e) => {
        announceResults(e.detail.teamWinner)
    }, false);
    scoreNotification.dispatchEvent(scoredEvent)
}

function announceResults(teamWin) {
    let winner = teamWin;
    if (winner === 'a') {
        scoreNotification.classList.add('teamAscore'); 
    } else if (winner === 'b') {
        scoreNotification.classList.add('teamBscore'); 
    }
    setTimeout(() => {
        scoreNotification.className = ''
    }, notificationTimeout);
}

court.addEventListener('click', e => moveBall(e), false);
aTeamScoringZone.addEventListener('click', () => teamScored(teamB), false)
bTeamScoringZone.addEventListener('click', () => teamScored(teamA), false)

/* END TASK 3 */
