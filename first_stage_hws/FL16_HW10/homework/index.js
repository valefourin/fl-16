
function isEqual(a, b) {
    return a === b;
}

function isBigger(a, b) {
    return a > b;
}

function storeNames(...arr) {
    return arr;
}

function getDifference(a, b) {
    if (a > b) {
        return a - b;
    } else {
        return b - a;
    }
}

function negativeCount(arr) {
    let count = 0;

    for (let el of arr) {
        if (el < 0) {
            count++;
        }
    }

    return count;
}

function letterCount(string, letter) {
    let count = 0;

    for (let el of `${string}`) {
        if (el === `${letter}`) {
            count++;
        }
    }

    return count;
}

function countScores(arr) {
    let count = 0, winCaseIncrement = 3, sep = ':';
    let teamX, teamY;

    for (let i = 0; i < arr.length; i++) {
        teamX = arr[i].split(sep)[0];
        teamY = arr[i].split(sep)[1];
        
        if (teamX > teamY) {
            count += winCaseIncrement;
        } else if (teamX === teamY) {
            count += 1;
        }
    }

    return count;
}
