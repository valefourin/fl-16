const root = document.getElementById('root');
const alertTime = 2000;
let tweets = []
let tweetItems = document.getElementById('tweetItems'),
    modifyItem = document.getElementById('modifyItem'),
    modifyItemHeader = document.getElementById('modifyItemHeader');

let textarea = document.getElementById('modifyItemInput'),
    btnAddTweet = document.getElementsByClassName('addTweet')[0],
    alertMsg = document.getElementById('alertMessage'),
    alertMsgText = document.getElementById('alertMessageText'),
    list = document.getElementById('list'),
    navigationBtns = document.getElementById('navigationButtons');

let btnGoToLiked = document.createElement('button');
    btnGoToLiked.innerText = 'Go to liked';
    hide(btnGoToLiked);
    navigationBtns.appendChild(btnGoToLiked);

let btnBack = document.createElement('button');
    btnBack.innerText = 'Back';
    hide(btnBack);
    navigationBtns.appendChild(btnBack);

let formButtons = document.getElementsByClassName('formButtons')[0],
    btnCancel = document.getElementById('cancelModification'),
    btnSave = document.getElementById('saveModifiedItem');

let btnEdit = document.createElement('button');
    btnEdit.innerText = 'Save Changes';
    hide(btnEdit);
    formButtons.appendChild(btnEdit);
let currentEditingTweet;

root.classList.add('center-aligned');
tweetItems.classList.add('center-aligned');
modifyItem.classList.add('center-aligned');

let likedTweetsIndexes = [];

function hide(node) {
    node.classList.add('hidden');
}

function show(node) {
    node.classList.remove('hidden');
}

function changeHash(str) {
    window.location.hash = str;
} 

function saveTweetsToLS() {
    localStorage.setItem('tweets', tweets);
}

function validateTweet(tweet) {
    if (tweet.length === 0) {
        return false;
    } else if (tweets.includes(tweet)) {
        return false;
    } else {
        return true;
    }
}

function addTweetPage() {
    history.pushState(location.hash, 'Add Tweet', location.href);
    modifyItemHeader.innerText = 'Add Tweet';
    show(modifyItem);
    hide(tweetItems);
    show(btnSave);
    hide(btnEdit);
    changeHash('/add');
}

function saveTweet() {
    let tweet = textarea.value;
    let valid = validateTweet(tweet);
    if (valid) {
            tweets.push(tweet);
            saveTweetsToLS();
            returnToMain();
    } else {
        showAlert("Error! You can't tweet about that");
    }
}

function editTweetPage(event,index) {
    history.pushState(location.hash, 'Edit Tweet', location.href);
    currentEditingTweet = tweets.indexOf(event.currentTarget.innerText);
    modifyItemHeader.innerText = 'Edit Tweet';
    textarea.value = tweets[index];
    show(modifyItem);
    hide(tweetItems);
    hide(btnSave);
    show(btnEdit);
    changeHash(`/edit/:${index}`);
    
}

function editTweet() {
    let tweet = textarea.value;
    let valid = validateTweet(tweet);
    if (valid) {
            tweets[currentEditingTweet] = tweet;
            saveTweetsToLS();
            returnToMain();
    } else {
        showAlert("Error! You can't tweet about that");
    }
}

function likeTweetPage() {
    history.pushState(location.hash, 'Like Tweet', location.href);
    modifyItemHeader.innerText = 'Liked Tweets';
    btnAddTweet.classList.add('hidden');
    btnGoToLiked.classList.add('hidden');
    btnBack.classList.remove('hidden');
    hide(modifyItem);
    show(tweetItems);
    changeHash('/liked');
    renderLikedTweets();
}

function likeTweet(event, index) {
    if (!likedTweetsIndexes.includes(index)) {
        likedTweetsIndexes.push(index);
    } 

    if (event.currentTarget.classList.contains('unliked')) {
        showAlert(`Hooray! You liked tweet with id ${index}!`);
        event.currentTarget.classList.remove('unliked');
        event.currentTarget.classList.add('liked')
    } else if (event.currentTarget.classList.contains('liked')) {
        showAlert(`Sorry you no longer like tweet with id ${index}!`);
        event.currentTarget.classList.remove('liked');
        event.currentTarget.classList.add('unliked')
    }
    let liked = document.getElementsByClassName('liked');

    if (liked.length > 0) {
        if (window.location.hash.includes('liked')){
            return;
        }
        btnGoToLiked.classList.remove('hidden');
    } else {
        btnGoToLiked.classList.add('hidden');
    }
}

function removeTweet(index) {
    tweets.splice(index, 1);
    saveTweetsToLS();
    renderTweets();
}

function renderTweets() {
    if (localStorage.getItem('tweets') !== null) {
        tweets = localStorage.getItem('tweets').split(',');
    }
    list.innerHTML = '';

    for (let i = 0; i < tweets.length ; i++) {
        let tweetButtons = document.createElement('span'),
            removeTweetBtn = document.createElement('button'),
            likeTweetBtn = document.createElement('span');

        removeTweetBtn.innerText = 'remove';
        likeTweetBtn.classList.add('heart');
        likeTweetBtn.classList.add('unliked');

        let li = document.createElement('li');
        let tweetText = document.createElement('span');
        tweetText.innerText = tweets[i];
        li.appendChild(tweetText);
        tweetButtons.appendChild(removeTweetBtn);
        tweetButtons.appendChild(likeTweetBtn);
        li.appendChild(tweetButtons);

        removeTweetBtn.addEventListener('click', () => removeTweet(i), false);
        likeTweetBtn.addEventListener('click', (e) => likeTweet(e,i), false);
        tweetText.addEventListener('click', (e) => editTweetPage(e,i), false);

        list.appendChild(li);
    }
}

function renderLikedTweets() {
    list.innerHTML = '';

    for (let i of likedTweetsIndexes) {
        let tweetButtons = document.createElement('span'),
            removeTweetBtn = document.createElement('button'),
            likeTweetBtn = document.createElement('span');

        removeTweetBtn.innerText = 'remove';
        likeTweetBtn.classList.add('heart');
        likeTweetBtn.classList.add('liked');

        let li = document.createElement('li');
        let tweetText = document.createElement('span');
        tweetText.innerText = tweets[i];
        li.appendChild(tweetText);
        tweetButtons.appendChild(removeTweetBtn);
        tweetButtons.appendChild(likeTweetBtn);
        li.appendChild(tweetButtons);

        removeTweetBtn.addEventListener('click', () => removeTweet(i), false);
        likeTweetBtn.addEventListener('click', (e) => likeTweet(e,i), false);
        tweetText.addEventListener('click', () => editTweetPage(i), false);

        list.appendChild(li);
    }
}

function returnToMain() {
    history.pushState(location.hash, 'Simple Twitter', location.href);
    hide(modifyItem);
    show(tweetItems);
    textarea.value = '';
    
    if (btnAddTweet.classList.contains('hidden')) {
        btnBack.classList.add('hidden');
        btnAddTweet.classList.remove('hidden');
    }

    renderTweets();
}

function showAlert(text) {
    alertMsg.classList.add('alertMessage');
    alertMsg.classList.remove('hidden');
    alertMsgText.innerText = text;
    setTimeout(() => {
        alertMsg.classList.add('hidden');
    }, alertTime);
}

window.onload = function() {
    btnAddTweet.addEventListener('click', addTweetPage, false);
    btnSave.addEventListener('click', saveTweet, false);
    btnEdit.addEventListener('click', editTweet, false)
    btnGoToLiked.addEventListener('click', likeTweetPage, false);
    btnBack.addEventListener('click', returnToMain, false);
    btnCancel.addEventListener('click', returnToMain, false);
    renderTweets();
}