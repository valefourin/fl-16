

const eighteen = 18, ten = 10, three = 3, minusOne = -1;

function reverseNumber(num) {
    if (Number.isInteger(num)) {
        let reversedNum = 0;
    let negative = false;
    
    if (Number(num) < 0) {
        num *= minusOne;
        negative = true;
    }

    while (num !== 0) {
        reversedNum *= ten;
        reversedNum += num % ten;
        num /= ten;
        num = Math.floor(num);
    }

    if (negative) {
        reversedNum *= minusOne;
    }

    return reversedNum;
    } else {
        console.warn('Failed to calculate. Please input an integer and refresh the page!');
    }
}

function forEach(arr, func) {
    for (let i = 0; i < arr.length; i++) {
        func(arr[i]);
    }
}

function map(arr, func) {
    let newArr = [];
    forEach(arr, function(el) {
        el = func(el);
        newArr.push(el);
    })
    return newArr;
}

function filter(arr, func) {
    let filteredArr = [];
    forEach(arr, function(el) {
        isValid = func(el);
        if (isValid) {
            filteredArr.push(el);
        }
    })
    return filteredArr;
}

function getAdultAppleLovers(data) {
    let adultAppleLovers = filter(data, function(person) {
        return person.age > eighteen && person.favoriteFruit === 'apple';
    });
    let names = map(adultAppleLovers, function(el) {
        return el.name;
    })
    return names;
}

function getKeys(obj) {
    let keys = [];
    for (el in obj) {
        if ({}.hasOwnProperty.call(obj, el)) {
            keys.push(el);
        }
    }
    return keys;
}

function getValues(obj) {
    let values = [];
    for (el in obj) {
        if ({}.hasOwnProperty.call(obj, el)) {
            values.push(obj[el]);
        }
    }
    return values;
}

function showFormattedDate(dateObj) {
    let day = dateObj.getDate(), 
    month = new Intl.DateTimeFormat('en-US', { month: 'long'} ).format(dateObj).slice(0, three), 
    year = dateObj.getFullYear();
    return `It is ${day} of ${month}, ${year}`;
}