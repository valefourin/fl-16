// Your code goes here

const list = document.getElementById('user-list');
const url = 'https://jsonplaceholder.typicode.com/users';
const spinner = document.querySelector('#spinner');
let userArr = [];

const getUsers = async () => {
    spinner.classList.remove('hidden');
    await fetch(url)
    .then(response => response.json())
    .then(data => 
        data.forEach(user => {
            userArr.push({
                id: user.id,
                name: user.name,
                username: user.username,
                email: user.email,
                phone: user.phone
            })
        })
    )
    .then(() => {
        spinner.classList.add('hidden');
        renderUsers();
    });
}

const updateUser = (user, fieldset) => {
    spinner.classList.remove('hidden');
     fetch(`${url}/${user.id}`, {
        method: 'PUT',
        headers: {
            'Content-type': 'application/json; charset=UTF-8' 
           },
        body: JSON.stringify({ user }) 
    })
    .then(response => {
        spinner.classList.add('hidden');
        return response.json();
    })
    .then(json => console.log(json));
    fieldset.setAttribute('disabled', true);
};

const deleteUser = async (id) => {
    spinner.classList.remove('hidden');
    let removedUser = document.getElementById(`user-${id}`);
    removedUser.remove();
    await fetch(`${url}/${id}`, {method: 'DELETE'})
    .then(() => 
        spinner.classList.add('hidden')
    );
    userArr = userArr.filter(user => user.id !== id);
} 

function renderUsers() {
    list.innerHTML = '';
    userArr.forEach(el => {
        let row = document.createElement('tr');
        row.setAttribute('id', `user-${el.id}`);
        let fieldset = document.createElement('fieldset');

        fieldset.setAttribute('id', `fieldset-${el.id}`);
        fieldset.setAttribute('disabled', 'true');

        let tdBtn = document.createElement('td');
        let editBtn = document.createElement('button');
        let saveBtn = document.createElement('button');
        let deleteBtn = document.createElement('button');

        deleteBtn.addEventListener('click', () => deleteUser(el.id))
        editBtn.addEventListener('click', () => {
            saveBtn.classList.remove('hidden');
            editBtn.classList.add('hidden');
            fieldset.removeAttribute('disabled');
        });
        saveBtn.addEventListener('click', () => {
            updateUser(el, fieldset);
            saveBtn.classList.add('hidden');
            editBtn.classList.remove('hidden');
        })

        editBtn.innerText = 'Edit';
        saveBtn.innerText = 'Save';
        saveBtn.classList.add('hidden');
        deleteBtn.innerText = 'Delete';


        for (const key in el) {
            if (el.hasOwnProperty(key)) {
                const td = document.createElement('td');
                const item = document.createElement('input');
                
                if (key === 'id') {
                    item.setAttribute('disabled', `true`);
                  }
                item.value = el[key];
                item.addEventListener('blur', () => {
                    el[key] = item.value;
                });

                td.append(item);
                fieldset.append(td)
            }
        }

        tdBtn.appendChild(editBtn);
        tdBtn.appendChild(saveBtn);
        tdBtn.appendChild(deleteBtn);
        row.appendChild(fieldset);
        row.appendChild(tdBtn);

        list.appendChild(row);
    })
}

getUsers();