const socket = new WebSocket.Server({ port: 8080 });

const minTwoDigitNum = 10;

const username = prompt('What is your name?')

function getCurrentTime() {
    let hours = new Date().getHours();
    let minutes = new Date().getMinutes();
    minutes = minutes < minTwoDigitNum ? `0${minutes}` : `${minutes}`;
    return `${hours}:${minutes}`;
}

document.forms.chat.onsubmit = function() {
    let outgoingMsg = this.message.value;
    let data = JSON.stringify({outgoingMsg, username, getCurrentTime})
    console.log(outgoingMsg)
    socket.send(data);
    return false;
};

socket.onmessage = function(event) {
    let incomingMsg = event.data;
    console.log(incomingMsg)
    showMsg(incomingMsg);
};

function showMsg(message) {
    let messageElem = document.createElement('div');
    messageElem.appendChild(document.createTextNode(message));
    document.getElementById('screen').appendChild(messageElem)
}