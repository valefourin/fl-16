// Write your code here

let operationStorage = ['+', '-', '/', 'x'], equationStorage, resultStorage = 0;
const logs = document.getElementById('logs');
const whole = 100, lastIndex = -1;

$(document).ready(function() {
        
    function evil(fn) {
        return new Function('return ' + fn)();
    }

    function inputDigit() {
        if ($('.screen').hasClass('attention-text')) {
            $('.screen').removeClass('attention-text');
            erase();
        }
        if ($('.screen').text() === 0) {
            $('.screen').text('')
        }
        $('.screen').append(+`${$(this).text()}`);
        equationStorage = $('.screen').text()
    }

    function inputOperator() {
        if (operationStorage.includes($('.screen').text().slice(lastIndex))) {
            $('.screen').text($('.screen').text().slice(0, $('.screen').text().length - 1))
        }
        $('.screen').append(`${$(this).text()}`);
        equationStorage = $('.screen').text();
    }

    function evaluate() {
        equationStorage = equationStorage.replace('x','*');
        if ($('.screen').text().includes('/0')) {
            $('.screen').text('ERROR');
            $('.screen').addClass('attention-text');
        } else {
            resultStorage = Math.round(evil(equationStorage) * whole) / whole;
            $('.screen').text(resultStorage);
            createLog(equationStorage, resultStorage);
        }
    }

    function erase() {
        equationStorage = '', resultStorage = 0;
        $('.screen').text(resultStorage);
    }

    function createLog(equation, result) {
        $('#logs').prepend(`
        <div class="log">
            <div class="circle"></div>
            <div class="log-text">${equation}=${result}</div>
            <div class="cross">&#10799;</div>
         </div>`);

        $(".log-text:contains('48')").css('text-decoration', 'underline');
    }

    function deleteLog(e) {
        $(e.target).parent().remove();
    }

    
    $('.digit-btn').on('click', inputDigit);
    $('.operation-btn').on('click', inputOperator);
    $('.evaluation-btn').on('click', evaluate);
    $('.erase-btn').on('click', erase);

    $(document).on('mouseenter', '.circle', e => {
        $(e.target).toggleClass('circle-clicked')
    });

    $(document).on('mouseleave', '.circle', e => {
        $(e.target).toggleClass('circle-clicked')
    });

    $(document).on('click', '.circle', e => {
        $(e.target).toggleClass('circle-clicked')
    });

    
    $(document).on('mouseenter', '.cross', e => {
        $(e.target).toggleClass('attention-text')
    });

    $(document).on('mouseleave', '.cross', e => {
        $(e.target).toggleClass('attention-text')
    });

    $(document).on('click', '.cross', e => deleteLog(e));

    
})