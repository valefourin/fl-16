//write your code here

class State {
    constructor(name) {
        this.name = name;
    }
}

class ReadyForPushNotification extends State {
    constructor(magazine) {
        super('ReadyForPushNotification')
        this.magazine = magazine
    }

    publish(employee) {
        console.log(`Hello ${employee.name}.  You can't publish. We are creating publications now.`)
    }
    approve(employee) {
        console.log(`Hello ${employee.name}.  You can't approve. We don't have enough of publications.`)
    }
}

class ReadyForApprove extends State {
    constructor(magazine) {
        super('ReadyForApprove')
        this.magazine = magazine
    }

    publish(employee) {
        console.log(`Hello ${employee.name} You can't publish. We don't have a manager's approval.`);
    }
    approve(employee) {
        console.log(`Hello ${employee.name}. You've approved the changes.`);
        this.magazine.current = this.magazine.states.find(el => el.name === 'ReadyForPublish');
    }
}

class ReadyForPublish extends State {
    constructor(magazine) {
        super('ReadyForPublish')
        this.magazine = magazine
    }

    publish(employee) {
        const delay = 60000;
        console.log(`Hello ${employee.name} You've recently published publications.`);
        this.magazine.current = this.magazine.states.find(el => el.name === 'PublishInProgress'); 

        this.magazine.listOfFollowers.forEach(follower => {
            const update = this.magazine.listOfArticles.filter(article => follower.topics.includes(article.topic));
            update.forEach(el => {
                follower.updates.push(el.text)
            });
        });

        this.magazine.listOfFollowers.forEach(follower => follower.onUpdate())
        
        this.magazine.listOfArticles = [];
        this.magazine.listOfFollowers.forEach(follower => follower.resetTopics());
        setTimeout(() => {
            this.magazine.current = this.magazine.states.find(el => el.name === 'ReadyForPushNotification'); 
        }, delay);
    }

    approve(employee) {
        console.log(`Hello ${employee.name} Publications have been already approved by you.`);
    }
}

class PublishInProgress extends State {
    constructor(magazine) {
        super('PublishInProgress')
        this.magazine = magazine
    }
    publish(employee) {
        console.log(`Hello ${employee.name}.  While we are publishing we can't do any actions.`);
    }
    approve(employee) {
        console.log(`Hello ${employee.name}.  While we are publishing we can't do any actions.`);
    }
}

class Magazine {
    constructor() {
        this.state = 'ReadyForPushNotification',
        this.listOfEmployees = [],
        this.listOfArticles = [],
        this.listOfFollowers = [],
        this.states = [
             new ReadyForPushNotification(this),
             new ReadyForApprove(this),
             new ReadyForPublish(this),
             new PublishInProgress(this)
        ],
        this.current = this.states.find(el => el.name === 'ReadyForPushNotification');
    }

    addArticle(topic, text) {
        this.listOfArticles.push({topic, text});
    }

    addEmployee(employee) {
        this.listOfEmployees.push(employee);
    }

    addFollower(follower) {
        if (!this.listOfFollowers.includes(follower)) {
            this.listOfFollowers.push(follower);
        } else {
            for (let f in this.listOfFollowers) {
                if (this.listOfFollowers[f].name === follower.name) {
                    this.listOfFollowers[f].topics = follower.topics;
                   break; 
                }
            }
        }
    }

    unsubscribeFollower(follower) {
        if (!this.listOfFollowers.includes(follower)) {
            console.log(`${follower.name}, you are not subscribed!`)
            this.listOfFollowers.push(follower);
        } else {
            this.listOfFollowers = this.listOfFollowers.filter(el => el.name !== follower.name)
        }
    }

    getState() {
        return this.current.name;
    }
}

class MagazineEmployee {
    constructor(name, task, magazine) {
        this.name = name
        this.task = task
        this.magazine = magazine
    }

    addArticle(text) {
        const minArticleNumForPublishing = 5;
        if (this.task === 'manager') {
            console.log(`Hello ${this.name}. Sorry, you can't add articles.`)
        } else {
            this.magazine.addArticle(this.task, text);
        }

        if (this.magazine.listOfArticles.length >= minArticleNumForPublishing) {
            this.magazine.current = this.magazine.states.find(el => el.name === 'ReadyForApprove'); 
        }
    }

    publish() {
        if (this.task === 'manager') {
            console.log(`Hello ${this.name}. Sorry, you can't publish magazine.`)
        } else {
            this.magazine.current.publish(this);
        }
    }

    approve() {
        if (this.task !== 'manager') {
            console.log(`Hello ${this.name}. Sorry, you don't have permission to do this.`)
        } else {
            this.magazine.current.approve(this);
        }
    }
}

class Follower {
    constructor(name) {
        this.name = name
        this.topics = []
        this.updates = []
    }

    subscribeTo(magazine, topic) {
        if (this.topics.includes(topic)) {
            console.log(`${this.name}, you are already subscribed to ${topic}`)
        } else {
            this.topics.push(topic);
            magazine.addFollower(this);
        }
    }

    unsubscribeFromTopic(magazine, topic) {
        if (!this.topics.includes(topic)) {
            console.log(`${this.name}, you are not subscribed to ${topic}`)
        } else {
            this.topics = this.topics.filter(t => t !== topic);
            magazine.addFollower(this);
        }
    }

    unsubscribeFromMagazine(magazine) {
        magazine.unsubscribeFollower(this);
    }

    onUpdate() {
        this.updates.forEach(article => {
            console.log(`${article} ${this.name}`)
        })
    }

    resetTopics() {
        this.topics = [];
    }
}