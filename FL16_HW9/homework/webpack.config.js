const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const isDev = process.env.NODE_ENV === 'development';
const isProd = !isDev;

const filename = (ext) => isDev ? `[name].${ext}` : `[name].[contenthash].${ext}`;
module.exports = {
    cache: false,
    context: path.resolve(__dirname, 'src'),
    entry: './js/index.js',
    mode: 'development',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: `./js/${filename('js')}`
    },
    module: {
        rules: [
            {
            // test: /\.scss$/,
            test: /\.css$/i,
            use:
                [
                    MiniCssExtractPlugin.loader, 'css-loader'
                ]
            },
            {
                test: /\.s[ac]ss$/,
                use:
                    [
                        MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader',
                    ]
            }
        ]
    },
    module: {
        rules: [
            {
            test: /\.js$/,
            exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                presets: ['@babel/preset-env']
                }
            }
            }
        ]
    },
    plugins: [
        new HTMLWebpackPlugin({
            template: path.resolve(__dirname, 'src/index.html'),
            filename: 'index.html',
            minify: {
                collapseWhitespace: isProd,
            }
        }),
        new CleanWebpackPlugin(),
        new MiniCssExtractPlugin({
            filename: `./scss/${filename('css')}`
        })
    ]
}